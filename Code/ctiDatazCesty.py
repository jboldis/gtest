#!/usr/bin/python3

import os
import csv

def cesta(path):
    data_path=[]
    for root, dirs, files in os.walk(path):
        for _file in files:
            radek=root.split("\\")
            radek.append(_file)    
            data_path.append(radek)
    return data_path

def csv_writer(data, path):
    """
    Write data to a CSV file path
    """
    with open(path, "w", newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        for line in data:
            writer.writerow(line)

if __name__ == "__main__":
    readpath="."
    data = cesta(readpath)
    path = "output.csv"
    csv_writer(data, path)